<?php
/**
 * VANILLA EMPTY PROJECT USING
 *
 * SLIM FOR ROUTING
 * MUSTACHE FOR TEMPLATING
 * 
 *
 */
require_once(__DIR__."/vendor/autoload.php"); // autoload all dependencies
require_once(__DIR__."/middlewares/ExampleMiddleware.php");

$app = new \Slim\App(); // create the router
$cfg = new \Spot\Config(); // create the config for database


$app->add(new ExampleMiddleware()); // add an example middleware. Remove this.

// get the container
$container = $app->getContainer();

// Connect to database, first argumet specifies type of database. ex. mysqllite can be used. The rest is credentials
$cfg->addConnection('mysql', [
    'dbname' => 'spot',
    'user' => 'root',
    'password' => '',
    'host' => 'localhost',
    'driver' => 'pdo_mysql',
]);


$container['spot'] = function() use (&$cfg){ 
	$spot = new \Spot\Locator($cfg); 
    return $spot;
};

/**
 * Register Mustache view function in IOC container for rendering views. 
 *
 * Normal slim syntax is:   $response->getBody()->write("Hello, $name");
 * With mustache same is:   $this->view->render($response, 'Hello, {{name}}', [ 'name' => "test" ]);
 *
 * This makes separation of code and html very nice
 */
$container['view'] = function () {
    $view = new \Slim\Views\Mustache([
        //'cache' =>sys_get_temp_dir(),
        'loader' => new Mustache_Loader_FilesystemLoader(__DIR__.'/views'), // main view files
        'partials_loader' => new Mustache_Loader_FilesystemLoader(__DIR__.'/views/partials') // partial view files
    ]);

    return $view;
};

require_once(__DIR__."/routes.php");