**Boilerplate setup for Slim, mustache and spot**

The purpose of this project is to provide a setup with minimal dependencies and still is extendable for larger projects.
Slim is used for request routing. (https://www.slimframework.com/ )
Mustache is used for views, this is done by **danjam/slim-mustache-view package**. ( http://mustache.github.io/ )
Spot is used as a DB layer and ORM. It supports mysql, sqllite and mssql ( http://phpdatamapper.com/ )

---

## Dependencies

1. PHP (preferably 7+)
2. composer [https://getcomposer.org/download/]
3. (optional) A database, either mysql, sqllite or mssql
4) (optional) A webserver

---

## Installation

1. Clone this repo. 
2. Run composer install.
3. Chage database settings in "bootstrap.php" file. 
4. (optional) For an example user table, go to directory **"Entities"** and run **"Install_user.php"**. This will create the table for the user model. More about this later.
5. Run the application either by 
   1 - Adding it to your webserver config and use the **/public** folder as document root. 
   2 - Using PHPs built in webserver. From root folder of this project run **php -S localhost:80 -t public/**. This is very good for development purposes
6. Installation is completed!

---

## Create a new route and display content

All routes are in the file "routes.php" in the root folder. The index route looks like this:
```php
<?php
$app->get('/', function ($request, $response) {
	return $this->view->render($response,'home/index', ["name" => "Test"]);
});
```
If we break this down, first argument to **app->get** is the path this route is triggered on, in this case **"/"** which is the root of the website. 
Then comes a anonymous function where the first argument is the request object it contains information about the request, such as server settings, remote adress etc.
The second argument to the anonymous function is the respons object, it contains information about the HTTP response such as status code etc. Both the request and the response object are rarley used but must be passed on to the next route.

The return is a rendered view mustache view. It loaded from disk,  **views/home/index.mustache** but is accessed in code as 'home/index'. 
The third argument to the render is variables accessable in the templace. In this case you can write {{name}} in the mustache template to get the text **"Test"**

You can create subfolders within the **"views"** folder in order to structure your layout. If your subfolder is called **yourfolder** and the view called **yourfile.mustache** 
they can then be adresed in the renderer like, ```php return->$this->view->render($response, 'yourfolder/yourfile'); ```

Mustache is fully supported to look at mustache manual for all ways to modify templates. [http://mustache.github.io/]

Bet practice for the logic of template variables is to place code in classes in another folder. Try to have as little logic in the route as possible.

---

## Database handling

Spot is used as a database component ( http://phpdatamapper.com/ ). The setup is done in bootstrap.php. Spot has an ORM layer which requires you to write models, called **Entities** in Spot. 
**/Entities** are stored in the root folder.

## Create a new model

The full spot documentation is available at https://github.com/spotorm/spot2 

An example **Entity** called **User** is provided in the **Entity** folder. 

Here is the **Entitiy class** for **User** breaked down.
First some initialization:
```php
<?php

namespace Entity;

use Spot\EntityInterface as Entity; 
use Spot\MapperInterface as Mapper; // Since spot is a DataMapper ORM Every entity needs a mapper. We use the default but its possible to write own mappers if there is much logic in the Entity
```

Extend spots Enitiy class and we access this class as \Entity\User in other places.
```php
class User extends \Spot\Entity 
```
The name of the database table
```php
    protected static $table = 'users'; 
```
All the fields and their types and default values, etc. See spot documentation for all possible values.(https://github.com/spotorm/spot2 )
```php
    public static function fields()
    {
        return [
            'id'           => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'username'     => ['type' => 'string', 'required' => true, 'index' => true],
            'passhash'     => ['type' => 'text', 'required' => true],
            'status'       => ['type' => 'text', 'default' => 0],
            'date_created' => ['type' => 'datetime', 'value' => new \DateTime()]
        ];
    }
}
```

It is possible to have relations between the models. It wont be covered here in detail but is in spot documentation. Here is an example that could fit in the **User** **Entity**
```php
    public static function relations(Mapper $mapper, Entity $entity)
    {
        return [
            'comments' => $mapper->hasMany($entity, 'Entity\Post\Comment', 'post_id')->order(['date_created' => 'ASC']),
            'customer' => $mapper->belongsTo($entity, 'Entity\Customer', 'customer_id')
        ];
    }
```