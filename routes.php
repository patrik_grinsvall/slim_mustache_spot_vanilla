<?php
/**
 * Contain all routes
 */

// index
$app->get('/', function ($request, $response) {
	return $this->view->render($response,'home/index');
});

// insert into database
$app->get('/insert/{name}', function ($request, $response, $args) {
	$name = $request->name;

    return $this->view->render($response, 'user/new', [
        'name' => $name
    ]);
});

// run the app
$app->run();